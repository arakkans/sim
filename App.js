import React, { Component } from "react";
import { Root } from "native-base";
import HomeScreen from "./src/home/HomeScreen.js";
import ScanScreen from "./src/scan/ScanScreen";
import { StackNavigator } from "react-navigation";

const Main = StackNavigator({
  Home: {
    screen: HomeScreen
  },
  Scan: {
    screen: ScanScreen
  }
});

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });

    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return (
      <Root>
        <Main />
      </Root>
    );
  }
}
