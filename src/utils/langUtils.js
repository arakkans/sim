const labels = {
  en: {
    title: "SiHK Registration",
    programday: "PROGRAM DAY",
    day1: "Day 1",
    day2: "Day 2",
    day3: "Day 3",
    volunteerlogin: "VOLUNTEER LOGIN",
    email: "Email",
    password: "Password",
    login: "Login",
    invalidemail: "Invalid Email",
    incorrectpassword: "Incorrect Password",
    okay: "Okay",
    scan: "Scan QR Code",
    cancelscan: "Cancel Scan",
    invalidticketmessage:
      "Invalid e-Ticket. Please contact Registration Help Desk.",
    othererrormessage:
      "Unexpected Error. Please contact Registration Help Desk.",
    day1message:
      "Participant did not check in on Day 1. Please contact Registration Coordinators",
    day2message:
      "Participant did not check in on Day 2. Please contact Registration Coordinators",
    bothdaysmessage:
      "Participant did not check in on Day 1 and Day 2. Please contact Registration Coordinators",
    preferred: "Preferred",
    standard: "Standard",
    premium: "Premium",
    firstname: "First Name",
    lastname: "Last Name",
    eticket: "E Ticket",
    ticketid: "Ticket ID",
    registrationId: "Regn ID",
    checkin: "Check In",
    checkinstatus: "CHECKIN STATUS",
    checkedinat: "Checked in at ",
    searchforeticket: "Search for e Ticket",
    search: "Search"
  },
  cn: {
    title: "SiHK 註冊",
    programday: "節目日",
    day1: "第1天",
    day2: "第二天",
    day3: "第3天",
    volunteerlogin: "志願者登錄",
    email: "電子郵件",
    password: "密碼",
    login: "登錄",
    invalidemail: "無效郵箱地址",
    incorrectpassword: "密碼錯誤",
    okay: "好的",
    scan: "掃描二維碼",
    cancelscan: "取消掃描",
    invalidticketmessage: "無效的電子票信息",
    othererrormessage: "其他錯誤信息。",
    day1message: "第1天信息",
    day2message: "第2天信息",
    bothdaysmessage: "兩天信息",
    preferred: "優選",
    standard: "標準",
    premium: "首選",
    firstname: "名字",
    lastname: "姓",
    eticket: "E電子票",
    ticketid: "電子票號",
    registrationId: "電子票號",
    checkin: "報到",
    checkinstatus: "檢查狀態",
    checkedinat: "報導時間 ",
    searchforeticket: "Search for e Ticket",
    search: "Search"
  }
};

const getLabel = (lang, key) => labels[lang][key];

export default getLabel;
