import React from "react";
import { StatusBar, StyleSheet, Alert } from "react-native";
import {
  Container,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Button,
  Body,
  Content,
  Text,
  Form,
  Label,
  Segment,
  Item,
  Input,
  Toast,
  List,
  ListItem,
  Footer
} from "native-base";
import moment from "moment";

import getLabel from "../utils/langUtils";

const PASSWORD = "sim2018";
const APP_VERSION = "1";

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    state = {
      password: "",
      email: "",
      isEmailValid: null,
      isPwdValid: null,
      programDay: 1,
      lang: "en",
      showDaySelection: false
    };
    this.handleLogin = this.handleLogin.bind(this);
    this.onProgramDayChange = this.onProgramDayChange.bind(this);
    this.onLanguageChange = this.onLanguageChange.bind(this);
  }

  componentDidMount() {
    this.setState({
      programDay: this.getProgramDay(),
      lang: "en"
    });
  }

  getProgramDay() {
    let day = 1;
    const currentDate = moment(new Date()).format("YYYYMMDD");
    if (currentDate === "20180415") day = 2;
    return day;
  }

  handleLogin() {
    const email = this.state ? this.state.email : "";
    const password = this.state && this.state.password;
    const programDay = this.state && this.state.programDay;
    const lang = (this.state && this.state.lang) || "en";
    if (!this.validateEmail(email)) {
      Toast.show({
        text: getLabel(lang, "invalidemail"),
        position: "top",
        buttonText: getLabel(lang, "okay"),
        duration: 5000,
        type: "danger"
      });
      this.setState({ isEmailValid: false });
      return;
    }
    if (password !== PASSWORD) {
      Toast.show({
        text: getLabel(lang, "incorrectpassword"),
        position: "top",
        buttonText: getLabel(lang, "okay"),
        duration: 5000,
        type: "danger"
      });
      this.setState({ isPwdValid: false });
      return;
    }
    this.setState({ isPwdValid: null, isEmailValid: null });
    this.props.navigation.navigate("Scan", {
      email,
      programDay,
      lang
    });
  }

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  onProgramDayChange(day) {
    this.setState({
      programDay: day
    });
  }

  onLanguageChange() {
    const lang = this.state && this.state.lang;
    this.setState({
      lang: lang === "en" ? "cn" : "en"
    });
  }

  onPasswordChange(text) {
    this.setState({
      password: text,
      isPwdValid: null,
      showDaySelection: text === "showmedays"
    });
  }

  render() {
    const isEmailValid = this.state && this.state.isEmailValid;
    const isPwdValid = this.state && this.state.isPwdValid;
    const programDay = this.state && this.state.programDay;
    const lang = (this.state && this.state.lang) || "en";
    const showDaySelection = this.state && this.state.showDaySelection;
    return (
      <Container style={styles.container}>
        {showDaySelection ? (
          <List>
            <ListItem itemDivider style={{ marginTop: 20 }}>
              <Text style={{ fontWeight: "bold" }}>
                {getLabel(lang, "programday")}
              </Text>
            </ListItem>
          </List>
        ) : null}
        {showDaySelection ? (
          <Segment style={{ marginTop: 10 }}>
            <Button
              first
              active={programDay === 1}
              onPress={() => this.onProgramDayChange(1)}
            >
              <Text>{getLabel(lang, "day1")}</Text>
            </Button>
            <Button
              active={programDay === 2}
              onPress={() => this.onProgramDayChange(2)}
            >
              <Text>{getLabel(lang, "day2")}</Text>
            </Button>
          </Segment>
        ) : null}
        <List>
          <ListItem itemDivider style={{ marginTop: 30 }}>
            <Text style={{ fontWeight: "bold" }}>
              {getLabel(lang, "volunteerlogin")}
            </Text>
          </ListItem>
        </List>
        <Content padder>
          <Form>
            <Item floatingLabel>
              <Label>{getLabel(lang, "email")}</Label>
              <Input
                keyboardType="email-address"
                onChangeText={text =>
                  this.setState({ email: text, isEmailValid: null })}
              />
            </Item>
            <Item floatingLabel last>
              <Label>{getLabel(lang, "password")}</Label>
              <Input
                secureTextEntry={true}
                onChangeText={text => this.onPasswordChange(text)}
              />
            </Item>
          </Form>
          <Button
            large
            full
            rounded
            primary
            style={{ marginTop: 20 }}
            onPress={() => this.handleLogin()}
          >
            <Text>{getLabel(lang, "login")}</Text>
          </Button>
        </Content>

        <Footer>
          <Left>
            <Text
              style={styles.versionLabel}
            >{`Version 0.0.${APP_VERSION}`}</Text>
          </Left>
          <Right>
            <Button
              small
              dark
              style={styles.languageButton}
              onPress={this.onLanguageChange}
            >
              <Text>{lang === "cn" ? "English" : "中文"}</Text>
            </Button>
          </Right>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  versionLabel: {
    marginLeft: 10
  },
  languageButton: {
    marginRight: 10
  }
});

HomeScreen.navigationOptions = {
  title: "SiM Registration"
};
