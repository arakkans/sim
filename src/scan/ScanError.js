import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import {
  Body,
  Title,
  Card,
  CardItem,
  Content,
  Icon,
  Button,
  Text,
  H3
} from "native-base";

class ScanError extends Component {
  render() {
    const { eTicket, errorMessage } = this.props;
    return (
      <Card style={styles.errorCard}>
        <CardItem style={styles.fullWidth}>
          <Body style={styles.errorIcon}>
            <Icon
              ios="ios-warning"
              android="md-warning"
              style={{ fontSize: 50, color: "red", marginBottom: 0 }}
            />
          </Body>
        </CardItem>
        <CardItem style={styles.fullWidth}>
          <Text style={styles.eTicket}>{eTicket}</Text>
        </CardItem>
        <CardItem>
          <Body>
            <H3>{errorMessage}</H3>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  fullWidth: { flex: 1 },
  errorIcon: { flex: 1, alignItems: "center" },
  eTicket: {
    fontWeight: "bold",
    fontSize: 28
  },
  errorCard: {
    marginTop: 10
  }
});

export default ScanError;
