import React, { Component } from "react";
import { StyleSheet, View, Vibration } from "react-native";
import { DangerZone } from "expo";
const { Lottie } = DangerZone;
import {
  Body,
  Title,
  Card,
  CardItem,
  Content,
  Icon,
  Button,
  Text,
  Form,
  Item,
  Label,
  Input,
  Left,
  Right,
  List,
  ListItem,
  Badge
} from "native-base";
import moment from "moment";
import keys from "lodash/keys";
import getLabel from "../utils/langUtils";

import done_animation from "../gif/check.json";

class ParticpantInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowingAnimation: false
    };
  }

  getDay(time) {
    time = time.toString();
    let day = "Invalid Day";
    if (time === "20180416") {
      day = "Day 3";
    } else if (time === "20180415") {
      day = "Day 2";
    }
    return day;
  }

  componentDidMount() {
    const { participant, programDate } = this.props;
    console.log("mounted");
    console.log(participant);
    if (participant) {
      this.updateCheckinHistory(participant, programDate);
      this.updateCheckinEnabledStatus(participant, programDate);
    }
    this.setState({
      isShowingAnimation: false
    });
  }
  componentWillReceiveProps(nextProps) {
    
    console.log(nextProps);
    const { participant, programDate } = nextProps;
    if (participant) {
      this.updateCheckinHistory(participant, programDate);
      this.updateCheckinEnabledStatus(participant, programDate);
    }
  }

  updateCheckinHistory(participant, programDate) {
    const checkins = keys(participant.checkins);
    const checkInHistory = checkins.reverse().map((key, index) => {
      console.log(key);
      const value = participant.checkins[key];
      return {
        key,
        time: moment(value.timestamp).format("h:mm A"),
        user: value.user,
        timestamp: value.timestamp,
        day: this.getDay(key)
      };
    });
    this.setState({ checkInHistory });
  }

  updateCheckinEnabledStatus(participant, programDate) {
    const { lang } = this.props;
    console.log(participant);    
    this.setState({
      isCheckinEnabled: participant.checkinAllowed,
      checkinDisabledMessage: participant.checkinAllowed ? '' : participant.checkinReason
    });
  }

  playAnimation() {
    this.isShowingAnimation = true;
    this.setState(
      {
        isShowingAnimation: true
      },
      () => {
        if (this.animation) this.animation.play();
        Vibration.vibrate();
      }
    );
  }

  animationStyle() {
    return {
      backgroundColor: "#fff",
      alignItems: "center",
      height: this.state.isShowingAnimation ? 150 : 0,
      marginTop: this.state.isShowingAnimation ? -40 : 0,
      marginBottom: this.state.isShowingAnimation ? -40 : 0
    };
  }

  render() {
    const {
      eTicket,
      participant,
      checkIn,
      isFetching,
      programDate,
      lang
    } = this.props;

    const isCheckinEnabled = this.state && this.state.isCheckinEnabled;
    const checkInHistory = (this.state && this.state.checkInHistory) || [];
    const errorMessage = this.state && this.state.checkinDisabledMessage;

    return (
      <Card style={styles.participantCard}>
        {true ? (
          <View style={this.animationStyle()}>
            <View
              style={{
                width: 200,
                height: 150
              }}
            >
              <Lottie
                ref={animation => {
                  this.animation = animation;
                }}
                style={{
                  width: 200,
                  height: 150,
                  backgroundColor: "#fff"
                }}
                source={done_animation}
              />
            </View>
          </View>
        ) : null}

        <CardItem style={styles.fullWidth}>
          <Text style={styles.eTicket}>{eTicket}</Text>
          {eTicket.startsWith("R") ? (
            <CardItem style={styles.fullWidth}>
              <Badge success>
                <Text style={styles.category}>
                  {getLabel(lang, "preferred")}
                </Text>
              </Badge>
            </CardItem>
          ) : null}
          {eTicket.startsWith("S") ? (
            <CardItem style={styles.fullWidth}>
              <Badge info>
                <Text style={styles.category}>
                  {getLabel(lang, "standard")}
                </Text>
              </Badge>
            </CardItem>
          ) : null}
          {eTicket.startsWith("P") ? (
            <CardItem style={styles.fullWidth}>
              <Badge warning>
                <Text style={styles.category}>{getLabel(lang, "premium")}</Text>
              </Badge>
            </CardItem>
          ) : null}
        </CardItem>

        <List>
          <ListItem>
            <Left>
              <Text>{getLabel(lang, "firstname")}</Text>
            </Left>
            <Body>
              <Text style={styles.formValue}>{participant.firstName}</Text>
            </Body>
          </ListItem>
          <ListItem>
            <Left>
              <Text>{getLabel(lang, "lastname")}</Text>
            </Left>
            <Body>
              <Text style={styles.formValue}>{participant.lastName}</Text>
            </Body>
          </ListItem>
          <ListItem>
            <Left>
              <Text>{getLabel(lang, "eticket")}</Text>
            </Left>
            <Body>
              <Text style={styles.formValue}>{participant.eTicketNo}</Text>
            </Body>
          </ListItem>
          <ListItem>
            <Left>
              <Text>{getLabel(lang, "registrationId")}</Text>
            </Left>
            <Body>
              <Text style={styles.formValue}>{participant.registrationId}</Text>
            </Body>
          </ListItem>
        </List>
        {isCheckinEnabled ? (
          <CardItem style={styles.fullWidth}>
            <Button
              rounded
              success
              style={styles.fullWidth}
              onPress={() => checkIn()}
              iconLeft
              block
              disabled={isFetching}
              large
            >
              <Icon ios="ios-checkmark-circle" android="md-checkmark-circle" />
              <Text>{getLabel(lang, "checkin")}</Text>
            </Button>
          </CardItem>
        ) : null}
        {errorMessage ? (
          <List>
            <ListItem>
              <View style={styles.errorMessageBox}>
                <Text style={styles.errorMessage}>{errorMessage}</Text>
              </View>
            </ListItem>
          </List>
        ) : null}
        {checkInHistory.length > 0 ? (
          <List style={{ marginTop: 10 }}>
            <ListItem itemDivider>
              <Text style={{ fontWeight: "bold" }}>
                {getLabel(lang, "checkinstatus")}
              </Text>
            </ListItem>
          </List>
        ) : null}
        {checkInHistory.map(item => {
          return (
            <List key={item.timestamp}>
              <ListItem icon>
                <Left>
                  <Icon
                    style={{ color: "green" }}
                    ios="ios-checkmark-circle"
                    android="md-checkmark-circle"
                  />
                </Left>
                <Body>
                  <Text style={{ fontWeight: "bold" }}>{`${item.day}`}</Text>
                </Body>
                <Right>
                  <Text>{`${getLabel(lang, "checkedinat")} ${item.time}`}</Text>
                </Right>
              </ListItem>
            </List>
          );
        })}
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  fullWidth: { flex: 1 },
  checkMark: { flex: 1, alignItems: "center" },
  timestamp: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    marginLeft: 40
  },
  participantCard: {
    marginTop: 10
  },
  eTicket: {
    fontWeight: "bold",
    fontSize: 28
  },
  category: {
    fontWeight: "bold"
  },
  formValue: {
    fontWeight: "bold"
  },
  errorMessageBox: {
    backgroundColor: "#C82736"
  },
  errorMessage: {
    color: "#FFFFFF",
    fontWeight: "bold",
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  animationContainer: {}
});

export default ParticpantInfo;
