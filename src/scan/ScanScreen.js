import React from "react";
import { AppRegistry, Alert, StyleSheet, View } from "react-native";
import { BarCodeScanner, Permissions, DangerZone } from "expo";
const { Lottie } = DangerZone;
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Card,
  CardItem,
  Content,
  Right,
  Icon,
  Button,
  Text,
  Footer,
  FooterTab,
  Spinner,
  List,
  ListItem,
  Form,
  Item,
  Label,
  Input
} from "native-base";

import ParticipantInfo from "./ParticipantInfo";
import ScanError from "./ScanError";
import moment from "moment";
import axios from "axios";
import "url-search-params-polyfill";
import getLabel from "../utils/langUtils";

const API_URL = "https://crms.ishayoga.sg/dev";
const API_KEY = "30LtZO8eTHF65sGjBxM0ILqOD0UBr70m";
const ERROR_CODE = "ERROR_RESOURCE_NOT_FOUND";
const SUCCESS_CODE = "SUCCESS";
const currentDay = "20171104"; //moment(new Date()).format("YYYYMMDD");

import loaidng_animation from "../gif/loading.json";

export default class Scan extends React.Component {
  constructor(props) {
    super(props);
    state = {
      isScanning: false,
      torchMode: false,
      isCheckin: false,
      eTicket: "",
      scanningError: false,
      participant: null,
      isFetching: false,
      errorMessage: null,
      programDay: 0,
      programDate: "",
      mode: 1,
      eTicketText: ""
    };
    this.checkIn = this.checkIn.bind(this);
    this.getProgramDate = this.getProgramDate.bind(this);
    this.changeMode = this.changeMode.bind(this);
    this.onEticketChange = this.onEticketChange.bind(this);
    this.onQueryEticket = this.onQueryEticket.bind(this);
  }

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
    if (this.animation) this.animation.play();
  }

  componentDidMount() {
    const programDay = this.props.navigation.state.params.programDay || 1;
    const programDate = this.getProgramDate(programDay);
    this.setState({
      programDay,
      programDate,
      mode: 1,
      eTicketText: ""
    });
    this.props.navigation.setParams({
      changeMode: this.changeMode,
      mode: 1
    });
  }

  getProgramDate(day) {
    let programDate = "20180414";
    if (day === 1) {
      programDate = "20180414";
    } else if (day === 2) {
      programDate = "20180415";
    }
    return programDate;
  }

  handleBarScodeScan = ({ type, data }) => {
    this.setState({
      isScanning: false,
      isCheckin: true,
      eTicket: data,
      isFetching: true
    });

    const { lang } = this.props.navigation.state.params;

    const params = new URLSearchParams();
    params.append("apiKey", API_KEY);
    params.append("action", "registration.getByETicketNo");
    params.append("eTicketNo", data);
    params.append("sessionDateYYYYMMDD", this.state.programDate)

    axios
      .post(API_URL, params)
      .then(response => {
        console.log(response);
        if (response.status !== 200) {
          this.setState({
            scanningError: true,
            errorMessage: getLabel(lang, "invalidticketmessage")
          });
        } else {
          const scanResponse = response.data;
          console.log(scanResponse);
          if (scanResponse.statusCode === SUCCESS_CODE) {
            console.log(scanResponse.object);
            this.setState({
              scanningError: false,
              isFetching: false,
              participant: scanResponse.object,
              mode: 1
            });
            this.props.navigation.setParams({ mode: 1 });
          } else {
            this.setState({
              scanningError: true,
              isFetching: false,
              errorMessage: getLabel(lang, "invalidticketmessage")
            });
          }
        }
      })
      .catch(error => {
        debugger
        this.setState({
          scanningError: true,
          isFetching: false,
          errorMessage: getLabel(lang, "othererrormessage")
        });
        console.log(error);
      });
   };

  checkIn() {
    const params = new URLSearchParams();
    params.append("action", "attendance.checkin");
    params.append("eTicketNo", this.state.participant.eTicketNo);
    params.append("apiKey", API_KEY);
    params.append("sessionDateYYYYMMDD", this.state.programDate);
    params.append(
      "user",
      this.props.navigation.state.params.email || "subeesh.a@gmail.com"
    );
    this.setState({ isFetching: true });
    axios
      .post(API_URL, params)
      .then(response => {
        if (response.status !== 200) {
          console.log(scanResponse.object);
          this.setState({ checkInError: true, isFetching: false });
        } else {
          const scanResponse = response.data;
          const { firstName, lastName, eTicketNo, registrationId} = this.state.participant
          const userInfo = {
            firstName,
            lastName,
            eTicketNo,
            registrationId
          }
          if (scanResponse.statusCode === SUCCESS_CODE) {
            console.log(scanResponse.object);
            this.setState({
              checkInError: false,
              isFetching: false,
              participant: Object.assign({}, userInfo, scanResponse.object)
            });
            if (this.participantInfo) this.participantInfo.playAnimation();
            const self = this;
            setTimeout(() => {
              this.setState({
                participant: null,
                isScanning: true
              });
            }, 2000);
          } else {
            console.log(scanResponse);
            this.setState({
              checkInError: true,
              isFetching: false,
              participant: null,
              errorMessage: scanResponse.userFriendlyMessage
            });
          }
        }
      })
      .catch(error => {
        this.setState({
          checkInError: true,
          isFetching: false
        });
        console.log(error);
      });
  }

  changeMode() {
    const mode = this.state.mode;
    this.setState(
      {
        mode: mode === 1 ? 2 : 1,
        participant: null,
        isScanning: mode === 2,
        isCheckin: false,
        scanningError: false,
        checkInError: false
      },
      () => {
        this.props.navigation.setParams({
          mode: this.state.mode
        });
      }
    );
  }

  onEticketChange(text) {
    this.setState({
      eTicketText: text
    });
  }

  onQueryEticket() {
    this.setState({
      participant: null
    });
    this.handleBarScodeScan({
      data: this.state.eTicketText.toUpperCase()
    });
  }

  render() {
    const torchMode = this.state && this.state.torchMode;
    const isScanning = this.state && this.state.isScanning;
    const isCheckin = this.state && this.state.isCheckin;
    const scanningError = this.state && this.state.scanningError;
    const checkInError = this.state && this.state.checkInError;
    const eTicket = this.state && this.state.eTicket;
    const participant = this.state && this.state.participant;
    const isFetching = this.state && this.state.isFetching;
    const errorMessage = this.state && this.state.errorMessage;
    const programDate = this.state && this.state.programDate;
    const programDay = this.state && this.state.programDay;
    const lang = this.props.navigation.state.params.lang;
    const mode = this.state && this.state.mode;
    return (
      <Container style={styles.container}>
        {mode === 2 ? (
          <View>
            <List>
              <ListItem itemDivider style={{ marginTop: 20 }}>
                <Text style={{ fontWeight: "bold" }}>
                  {"Search for eTicket"}
                </Text>
              </ListItem>
            </List>
            <Form>
              <Item floatingLabel>
                <Label>E Ticket Number</Label>
                <Input onChangeText={text => this.onEticketChange(text)} />
              </Item>
            </Form>
            <Button
              large
              primary
              iconLeft
              full
              rounded
              style={[styles.actionButton, { marginTop: 20 }]}
              onPress={() => this.onQueryEticket()}
            >
              <Text>{getLabel(lang, "search")}</Text>
            </Button>
          </View>
        ) : null}
        <Content padder>
          {scanningError || checkInError ? (
            <ScanError eTicket={eTicket} errorMessage={errorMessage} />
          ) : null}
          {participant ? (
            <ParticipantInfo
              eTicket={eTicket}
              participant={participant}
              checkIn={this.checkIn}
              isFetching={isFetching}
              programDate={programDate}
              programDay={programDay}
              lang={lang}
              ref={participantInfo => {
                this.participantInfo = participantInfo;
              }}
            />
          ) : null}
          {isFetching ? <Spinner color="green" /> : null}
        </Content>

        {!isScanning && mode === 1 ? (
          <Button
            large
            primary
            iconLeft
            full
            rounded
            style={styles.actionButton}
            onPress={() =>
              this.setState({
                isScanning: true,
                isCheckin: false,
                participant: null,
                scanningError: false,
                checkInError: false
              })}
          >
            <Icon ios="ios-qr-scanner" android="md-qr-scanner" />
            <Text>{getLabel(lang, "scan")}</Text>
          </Button>
        ) : null}
        {isScanning ? (
          <BarCodeScanner
            onBarCodeRead={this.handleBarScodeScan}
            style={StyleSheet.absoluteFill}
            barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
            torchMode={torchMode ? "on" : "off"}
          />
        ) : null}
        {isScanning ? (
          <Button
            danger
            full
            rounded
            onPress={() => this.setState({ isScanning: false })}
            large
            style={styles.actionButton}
          >
            <Text>{getLabel(lang, "cancelscan")}</Text>
          </Button>
        ) : null}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white"
  },
  torchButton: {
    position: "absolute",
    right: 20,
    bottom: 10
  },
  footerStyle: {
    backgroundColor: "rgba(0, 0, 0, 0)",
    borderStyle: "solid",
    borderWidth: 0,
    marginBottom: 30
  },
  actionButton: {
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20
  },
  animationContainer: {
    backgroundColor: "#fff",
    alignItems: "center",
    height: 125
  },
  overlayContainer: {
    position: "absolute",
    left: 0,
    top: 0,
    bottom: 0,
    right: 0
  },
  overlaySegment: {
    backgroundColor: "#0f0",
    position: "absolute"
  },
  overlayBottom: {
    bottom: 0,
    height: 200,
    left: 0,
    right: 0
  },
  overlayLeft: {
    height: 200,
    left: 0,
    width: 100,
    top: 200
  },
  overlayRight: {
    height: 200,
    right: 0,
    width: 100,
    top: 200
  },
  overlayTop: {
    height: 200,
    left: 0,
    right: 0,
    top: 0
  }
});

Scan.navigationOptions = ({ navigation }) => ({
  header: (
    <Header>
      <Body>
        <Title>SiM Registration</Title>
      </Body>
      <Right>
        <Right>
          <Button
            transparent
            onPress={() => navigation.state.params.changeMode()}
          >
            <Icon
              ios={
                navigation.state.params.mode === 1
                  ? "ios-search"
                  : "ios-qr-scanner"
              }
              android={
                navigation.state.params.mode === 1
                  ? "md-search"
                  : "md-qr-scanner"
              }
            />
          </Button>
        </Right>
      </Right>
    </Header>
  )
});

// <Button
// iconRight
// onPress={() => this.setState({ torchMode: !torchMode })}
// warning
// rounded
// style={styles.torchButton}
// >
// <Icon active={torchMode} name="light-bulb" />
// </Button>
